<div align="center">
<p align="center" >
<img src="./.gitlab/tempered-fox.png" width="60" />
</p>

<h1 align="center">Tempered Fox</h1>
<p align="center"> A firefox configuration template aiming for a balance between privacy,security & useability. </p>
</div>


## Credits 
This project wasn't completely made by me, and a lot of stuff has been added from the following projects :

* [Arkenfox](https://github.com/arkenfox)
* [LibreWolf](https://librewolf-community.gitlab.io/)
* [Pyllyukko's user.js ](https://github.com/pyllyukko/user.js)

<div>Icons used were made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

## License
This project has been licensed under the **MIT license**.
___
MIT License

Copyright (c) 2021 CTZ VULKAN

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

